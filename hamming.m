%Renan Bortoluzzi - 240509
%Obs: para fazer este trabalho troquei ideias com um colega da turma, dessa
%forma os codigos sao bem semlhante.
%tambem usei esta pagina como consulta: https://www.mathworks.com/matlabcentral/fileexchange/10774-hamming-7-4-code-simulation?focused=6236817&tab=function

clear all;
close all;

num_b = 3000000; %quantidade de bits simulados

% Quantidade bits da palavra - n
%Como solicitado no enunciado, o tamanho da palavra pode ser parametrizavel
tam_palavra = input('Tamanho da palavra: ');
% Quantidade bits da mensagem - k: 
%k = n - p
tam_msg = tam_palavra - log2(tam_palavra+1);

%bits = a + bi, onde a = {-1,1} e b = 0
bits_gerados = complex(2*randi(2, 1, num_b)-3, 0); %vetor complexo com parte real {-1, 1} e parte imaginaria = 0

%Manipula a lista de bist gerados para aplicar na funcao encode (0,1)
bits_gerados(bits_gerados==-1) = 0;

% Codifica dados usando Hamming
bits_codificados = encode(bits_gerados,tam_palavra, tam_msg, 'hamming/binary');

% Voltar para (-1, 1)
bits_codificados(bits_codificados==0) = -1;
bits_gerados(bits_gerados==0) = -1;

%%%% Variaveis padrao
Eb_N0_dB = 0:1:10; %faixa de Eb/N0 a ser simulada (em dB)
Eb_N0_lin = 10 .^ (Eb_N0_dB/10); %Eb/N0 linearizado
Eb = 1; %energia por s�mbolo e constante (1^2 = (-1)^2 = 1), 1 bit por s�mbolo (caso geral: energia media por s�mbolo / bits por s�mbolo)
NP = Eb ./ (Eb_N0_lin); %potencia do ru�do
NA = sqrt(NP); %amplitude e a raiz quadrada da potencia

ber_hamming = zeros(size(Eb_N0_lin)); %pre-allocates BER Hamming vector
ber = zeros(size(Eb_N0_lin)); %pre-allocates BER vector

for i = 1:length(Eb_N0_lin)
    HAMn = NA(i)*complex(randn(1, length(bits_codificados)), randn(1, length(bits_codificados)))*sqrt(0.5); %vetor de ru�do com desvio padrao igual � amplitude do ru�do
    n = NA(i)*complex(randn(1, num_b), randn(1, num_b))*sqrt(0.5); %vetor de ru�do com desvio padrao igual � amplitude do ru�do
    
    HAMr = bits_codificados + HAMn;
    r = bits_gerados + n; % canal AWNG
    
    demod = sign(real(r));
    HAMdemod = sign(real(HAMr)); % sinal da parte real determina o valor do bit

    % Decodifica�ao
    HAMdemod(HAMdemod==-1) = 0;
    bits_decodificados = decode(HAMdemod, tam_palavra, tam_msg, 'hamming/binary');
    bits_decodificados(bits_decodificados==0) = -1;
    
    % Calculo do BER
    ber_hamming(i) = sum(bits_decodificados(1:num_b) ~= bits_gerados) / num_b; % conta erros e calcula o BER
    ber(i) = sum(bits_gerados ~= demod) / num_b;
end

ber_theoretical = 0.5*erfc(sqrt(Eb_N0_lin)); %BER teorico

% Plot
semilogy(Eb_N0_dB, ber, Eb_N0_dB, ber_theoretical, Eb_N0_dB, ber_hamming, 'LineWidth', 2);
grid on;
title('Taxa de erros para BPSK');
legend('BPSK', 'Teorico', 'BPSK Hamming');
ylabel('BER');
xlabel('Eb/N0 (dB)');
